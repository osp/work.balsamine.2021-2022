
// mini GUY interface for pagedjs
// written by Dorian Timmermans

// DONE:
// * zoom level (that does not influence the interface)
// * save checkboxes state and scroll level through ctrl+R (put in url?)
// * interface outline have constant stroke width

// TODO:
// * go to page x button
// * start
// * don't spread if only 1 sheet
// * prevent classic zoom

// --- Start

// reload value from sessionStorage
// IMPORTANT NOTE: for that to work we have to call this function after pagedjs
// using the window.PagedConfig after parameter
// handler/hook or calling pagedjs async does not work!

function getSessionValue(valueName, defaultValue){
  let sessionValue = JSON.parse(sessionStorage.getItem(valueName));
  return sessionValue === null ? defaultValue : sessionValue;
}

let SPREAD = getSessionValue("spread", false);
let PREVIEW = getSessionValue("preview", false);
let ZOOM = getSessionValue("zoom", 1);
let PAGE = getSessionValue("page", 1);

let INTERFACE_CREATED = false;

function createZoomSlider(){
  // html
  let $container = $('<div>').addClass('control');
  let $slider = $('<input>').attr('type', 'range');
  let $label = $('<label>').html('Zoom');
  $container.append($slider);
  $container.append($label);

  $slider.attr('min', 0);
  $slider.attr('max', 2);
  $slider.attr('step', 0.05);
  $slider.attr('value', ZOOM);

  let $pages = $('.pagedjs_pages');
  $pages.css("transform-origin", "top left");

  function updateZoomSlider(val){
    // apply zoom
    $pages.css("transform", "scale(" + val + ")");
    // apply reverse zoom to interface outlines
    let val_inv = 1/val;
    $("html").attr("style","--interface-outline:"+ val_inv +"px");
    // set sessionStorage
    sessionStorage.setItem("zoom", val);
  }
  $slider.on('input', function(){
      let val = $(this).val();
      updateZoomSlider(val);
  });
  updateZoomSlider(ZOOM);

  return $container;
}


function createGoToPageButton(){
  // Note:
  // no need for sessionStorage because pages is already remembered by
  // scroll, but for button coherence so the value is the good one when refreshing

  // html
  let $container = $('<div>').addClass('control');
  let $number = $('<input>').attr('type', 'number');
  let $label = $('<label>').html('Pages');
  $container.append($number);
  $container.append($label);

  // values
  let pages = $(".pagedjs_page").length;
  $number.attr('min', 1);
  $number.attr('max', pages);
  $number.attr('step', 1);
  $number.attr('value', PAGE);

  $number.on('input', function(){
    let val = $(this).val();
    let offTop = $("#page-"+val).offset().top - 50;
    let offLeft = $("#page-"+val).offset().left - 50;
    $('html, body').scrollTop(offTop);
    $('html, body').scrollLeft(offLeft);
    sessionStorage.setItem("page", val);
  });

  return $container;
}

function createCheckboxControl(name, boolean){
  // html
  let $container = $('<div>').addClass('control');
  let $checkbox = $('<input>').attr('type', 'checkbox');
  let $label = $('<label>').html(name);
  $container.append($checkbox);
  $container.append($label);
  // toggle class on body, on state change
  $checkbox.on('change', function(){
    $('body').toggleClass(name);
    boolean = !boolean;
    sessionStorage.setItem(name, boolean);
  });
  // init
  if(boolean){
    $('body').addClass(name);
    $checkbox.prop('checked', true);
  }
  return $container;
}

function getImageDPI(img){
  // original size
  let nw = img.naturalWidth;
  let nh = img.naturalHeight;
  // size in px without border
  let w = img.clientWidth;
  let h = img.clientHeight;
  // printing size in cm
  // html conversion between pixel unit and cm is 96dpi
  // TODO: does this conversion change according to the user DPI ???!!!
  let pw = (w / 96) * 2.54;
  let ph = (h / 96) * 2.54;

  // DPI
  let dpiw = nw / (w / 96);
  let dpih = nh / (h / 96);

  // color profile
  exifr.parse(img.src, {tiff: false, icc: true}).then(output => {
    let ICC = output ? output.ProfileDescription : undefined;

    let split = img.src.split("/");
    let name = split[split.length-1];
    let console_object = {
      aa: name,
      naturalWidth: nw + "px",
      naturalHeight: nh + "px",
      printingWidth: pw.toFixed(2) + "cm",
      printingHeight: ph.toFixed(2) + "cm",
      dpiWidth: dpiw.toFixed(2) + "dpi",
      dpiHeight: dpih.toFixed(2) + "dpi",
      colorProfile: ICC
    }
    console.log(console_object);
  })

}

function showImagesDPI(){
  $('img').each(function(){
    // for each img that is not an svg
    if(!this.src.includes(".svg")){
      getImageDPI(this);
    }
  });
}

// --- remember scroll values through reload

window.addEventListener('scroll', function(){
  if (INTERFACE_CREATED){
    let x = Math.round(window.scrollX);
    let y = Math.round(window.scrollY);
    sessionStorage.setItem("scrollX", x);
    sessionStorage.setItem("scrollY", y);
  }
})

// --- init

function initInterface(){
  console.log("interface-start");


  // add the interface elements
  let $interface = $("<section>").attr('class', 'interface');
  $interface.append(createCheckboxControl('spread', SPREAD));
  $interface.append(createCheckboxControl('preview', PREVIEW));
  $interface.append(createGoToPageButton());
  $interface.append(createZoomSlider());
  $('body').prepend($interface);

  // pagedjs fix
  $interface.css('position','fixed');

  // finally set scroll values from sessionStorage
  let scrollX = sessionStorage.getItem("scrollX");
  let scrollY = sessionStorage.getItem("scrollY");
  window.scrollTo(parseInt(scrollX), parseInt(scrollY));

  // compute img dpi
  // showImagesDPI();

  INTERFACE_CREATED = true;
  console.log("interface-end");
}
