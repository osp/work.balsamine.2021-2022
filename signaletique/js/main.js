
function replace(selector, toreplace, replacewith){
  // remplace une string par une autre dans l'HTML
  // de l'élément selectionné
  let e_list = document.querySelectorAll(selector);

  // pour remplacer dans tout les éléments selectionnés
  for (var i = 0; i < e_list.length; i++) {
    // console.log(e_list[i].innerHTML);
    // pour remplacer toutes les occurences dans l'élément
    let regex = new RegExp(toreplace, "g");
    e_list[i].innerHTML = e_list[i].innerHTML.replace(regex, replacewith);
  }
}

function spanify(selector, tospanify){
  // encadre un truc de span
  replace(selector, tospanify, "<span>"+tospanify+"</span>");
}

function insertLogosRatio(selector){
  // ajoute une classe a tout les éléments contenu dans le div
  // en fonction de leur ratio
  let logos = $(selector).children("img");

  logos.each(function(){
    let w = $(this).width();
    let h = $(this).height();
    let ratio = w / h;
    if(ratio > 3){
      $(this).addClass("long-logo")
    }
    // console.log(w, h, ratio);
  });

}
