$(function() {
    $('iframe').load(function() {
        var doc = $("iframe").contents().find("html");

        $('[name="preview"]').change(function() {
            if($(this).is(":checked")) {
                doc.addClass("preview");
                doc.removeClass("normal");
            } else {
                doc.removeClass("preview");
                doc.addClass("normal");
            }
        });

        $('[name="debug"]').change(function() {
            if($(this).is(":checked")) {
                doc.addClass("debug");
            } else {
                doc.removeClass("debug");
            }
        });

        $('[name="spread"]').change(function() {
            if($(this).is(":checked")) {
                doc.addClass("spread");
            } else {
                doc.removeClass("spread");
            }
        });


        $('[name="zoom"]').change(function() {
            zoomLevel = $(this).val() / 100;

            doc.find("#pages").css({
                "-webkit-transform": "scale(" + zoomLevel + ")",
                "-webkit-transform-origin": "0 0"
            });
        });


        $('[name="page"]').change(function() {
            var pageNumber = $(this).val() - 1;

            var target = doc.find('.paper:eq(' + pageNumber + ')');
            var offsetTop = target.offset().top;

            doc.find('body').scrollTop(offsetTop);
        });

        $("#print").on('click', function() {
            $("iframe").get(0).contentWindow.print();
        });
			
			var $window = this.contentWindow;

			$window.addEventListener("scroll", function(event) {
				// save scroll position in variable
				
				scrollX = Math.round($window.scrollX);
				scrollY = Math.round($window.scrollY);
				console.log(scrollX, '----', scrollY);
				sessionStorage.setItem("scrollX", scrollX);
				sessionStorage.setItem("scrollY", scrollY);
			});
    });
    


});

