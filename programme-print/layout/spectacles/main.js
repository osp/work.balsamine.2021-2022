function flowTemplate(i) {
	var r = `
			#stories section:nth-of-type(`+i+`)  .image-cover{
				-webkit-flow-into: image-`+i+`;
				flow-into: image-`+i+`;
			}
			.article-`+i+` .image {
				-webkit-flow-from: image-`+i+`;
				flow-from: image-`+i+`;
			}
			#stories section:nth-of-type(`+i+`)  .image-cover figcaption,
			#stories section:nth-of-type(`+i+`)  .image-cover-festival figcaption
			{
				-webkit-flow-into: image-figcaption-`+i+`;
				flow-into: image-figcaption-`+i+`;
			}
			.article-`+i+` .image-figcaption {
				-webkit-flow-from: image-figcaption-`+i+`;
				flow-from: image-figcaption-`+i+`;
			}
			#stories section:nth-of-type(`+i+`) .image-extra figcaption
			{
				-webkit-flow-into: image-extra-figcaption-`+i+`;
				flow-into: image-extra-figcaption-`+i+`;
			}
			.article-`+i+` .image-extra-figcaption {
				-webkit-flow-from: image-extra-figcaption-`+i+`;
				flow-from: image-extra-figcaption-`+i+`;
			}
			#stories section:nth-of-type(`+i+`) .image-trait
			{
				-webkit-flow-into: image-trait-`+i+`;
				flow-into: image-trait-`+i+`;
			}
			.article-`+i+` .image-trait {
				-webkit-flow-from: image-trait-`+i+`;
				flow-from: image-trait-`+i+`;
			}
			#stories section:nth-of-type(`+i+`)  .phrase-intro{
				-webkit-flow-into: phrase-`+i+`;
				flow-into: phrase-`+i+`;
			}
			.article-`+i+` .phrase {
				-webkit-flow-from: phrase-`+i+`;
				flow-from: phrase-`+i+`;
			}
			#stories section:nth-of-type(`+i+`)  .image-cover-festival{
				-webkit-flow-into: image-`+i+`;
				flow-into: image-`+i+`;
			}
			.article-`+i+` .image-festival {
				-webkit-flow-from: image-`+i+`;
				flow-from: image-`+i+`;
			}
			#stories section:nth-of-type(`+i+`)  .image-forme{
				-webkit-flow-into: image-forme-`+i+`;
				flow-into: image-forme-`+i+`;
			}
			.article-`+i+` .image-forme {
				-webkit-flow-from: image-forme-`+i+`;
				flow-from: image-forme-`+i+`;
			}
			#stories section:nth-of-type(`+i+`)  .schedule__dates .date--detail {
				-webkit-flow-into: date-detail-`+i+`;
				flow-into: date-`+i+`;
			}
			.article-`+i+` .header-date-detail {
				-webkit-flow-from: date-detail-`+i+`;
				flow-from: date-`+i+`;
			}
			#stories section:nth-of-type(`+i+`)  .schedule__dates{
				-webkit-flow-into: date-`+i+`;
				flow-into: date-`+i+`;
			}
			.article-`+i+` .header-date {
				-webkit-flow-from: date-`+i+`;
				flow-from: date-`+i+`;
			}
			#stories section:nth-of-type(`+i+`) {
				-webkit-flow-into: main-`+i+`;
				flow-into: main-`+i+`;
			} 
			.article-`+i+` .body {
				-webkit-flow-from: main-`+i+`;
				flow-from: main-`+i+`;
			}
			.article-`+i+` [data-master="m-current"] .page::before,
			{
				content : "Auteur inconnu";
			}`;

			return r;
}

function postionPage(){
	document.cookie = "x=100";
	document.cookie = "y=500";
	let cookies = document.cookie.split(';')
}


var articles = []
articles[1] = ['m-image', 'm-first', 'm-current', 'm-current'] // AUTEUR ICONNU 
articles[2] = ['m-image', 'm-first'] // SCHEARBEEK FETE intro
articles[3] = ['m-first-festival'] // SCHEARBEEK FETE cruda
articles[4] = ['m-first-festival'] // SCHEARBEEK FETE mousse
articles[5] = ['m-first-festival', 'm-current'] // SCHEARBEEK FETE heureux
articles[6] = ['m-image', 'm-first', 'm-current'] // INTERIEUR
articles[7] = ['m-first-festival'] // INTERIEUR - installation
articles[8] = ['m-image', 'm-first', 'm-current', 'm-empty'] // OCTOBRE 1968 MEXICO
articles[9] = ['m-image', 'm-first', 'm-current', 'm-empty'] // LE PARADOXE DE BILLY
articles[10] = ['m-image', 'm-first', 'm-current', 'm-empty'] // LES LIANES
articles[11] = ['m-image', 'm-first', 'm-current'] // XX TIME
articles[12] = ['m-first-festival'] // XX TIME
articles[13] = ['m-first-festival'] // XX TIME
articles[14] = ['m-first-festival'] // XX TIME
articles[15] = ['m-first-festival'] // XX TIME
articles[16] = ['m-first-festival'] // XX TIME
articles[17] = ['m-first-festival'] // XX TIME
articles[18] = ['m-first-festival'] // XX TIME
articles[19] = ['m-image', 'm-first', 'm-current', 'm-empty'] // AUDREY LUCIE 
articles[20] = ['m-image', 'm-first', 'm-current', 'm-empty'] // TODOS CAERAN
articles[21] = ['m-image', 'm-first'] // STAND BY
articles[22] = ['m-image', 'm-first', 'm-current', 'm-empty'] // ABRI
articles[23] = ['m-image', 'm-first', 'm-current', 'm-empty'] // AU PIED DES MONTAGNES
articles[24] = ['m-first'] // partenariats
articles[25] = ['m-first', 'm-current'] // balsatoiles


window.addEventListener('DOMContentLoaded', function(){
	
	let styleFlow = document.getElementById('styleFlow')
	let styleFlow2 = document.getElementById('styleFlow2')

	console.log('flowStyleTest ---> ', styleFlow)

	for (let i = 1; i < 27; i++) {
		styleFlow.innerHTML += flowTemplate(i) 
	}


	var MASTERS = document.querySelectorAll('.master-page')
	var PAGES = document.querySelector('#pages')
	
	articles.forEach(function(item, i){
		item.forEach(function(page, u){
			var master = document.getElementById(page)
			var newPage = master.cloneNode(true)
			newPage.setAttribute('id', 'article-'+i+'--page-'+u)
			newPage.setAttribute('data-master', master.id)
			newPage.classList.remove('master-page')
			newPage.classList.add('article-'+i)
			PAGES.innerHTML += newPage.outerHTML

		})
	})

	
})

