
function replace(selector, toreplace, replacewith){
  // remplace une string par une autre dans l'HTML
  // de l'élément selectionné
  let e_list = document.querySelectorAll(selector);

  // pour remplacer dans tout les éléments selectionnés
  for (var i = 0; i < e_list.length; i++) {
    console.log(e_list[i].innerHTML);
    // pour remplacer toutes les occurences dans l'élément
    let regex = new RegExp(toreplace, "g");
    e_list[i].innerHTML = e_list[i].innerHTML.replace(regex, replacewith);
  }
}
