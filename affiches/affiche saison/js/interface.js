
// mini GUY interface for pagedjs
// written by Dorian Timmermans

// DONE:
// * zoom level (that does not influence the interface)
// * save checkboxes state and scroll level through ctrl+R (put in url?)
// * interface outline have constant stroke width

// TODO:
// * go to page x button
// * start
// * don't spread if only 1 sheet
// * prevent classic zoom

// --- Start

// reload value from sessionStorage
// IMPORTANT NOTE: for that to work we have to call this function after pagedjs
// using the window.PagedConfig after parameter
// handler/hook or calling pagedjs async does not work!
let SPREAD = sessionStorage.getItem("spread") === null ? false : (sessionStorage.getItem("spread") === "true");
let PREVIEW = sessionStorage.getItem("preview") === null ? false : (sessionStorage.getItem("preview") === "true");
let ZOOM = sessionStorage.getItem("zoom") === null ? 1 : sessionStorage.getItem("zoom");
let PAGE = sessionStorage.getItem("page") === null ? 1 : sessionStorage.getItem("page");

let INTERFACE_CREATED = false;

function createZoomSlider(){
  // html
  let $container = $('<div>').addClass('control');
  let $slider = $('<input>').attr('type', 'range');
  let $label = $('<label>').html('Zoom');
  $container.append($slider);
  $container.append($label);

  $slider.attr('min', 0);
  $slider.attr('max', 2);
  $slider.attr('step', 0.1);
  $slider.attr('value', ZOOM);

  let $pages = $('.pagedjs_pages');
  $pages.css("transform-origin", "top left");

  function updateZoomSlider(val){
    // apply zoom
    $pages.css("transform", "scale(" + val + ")");
    // apply reverse zoom to interface outlines
    let val_inv = 1/val;
    $("html").attr("style","--interface-outline:"+ val_inv +"px");
    // set sessionStorage
    sessionStorage.setItem("zoom", val);
  }
  $slider.on('input', function(){
      let val = $(this).val();
      updateZoomSlider(val);
  });
  updateZoomSlider(ZOOM);

  return $container;
}


function createGoToPageButton(){
  // Note:
  // no need for sessionStorage because pages is already remembered by
  // scroll, but for button coherence so the value is the good one when refreshing

  // html
  let $container = $('<div>').addClass('control');
  let $number = $('<input>').attr('type', 'number');
  let $label = $('<label>').html('Pages');
  $container.append($number);
  $container.append($label);

  // values
  let pages = $(".pagedjs_page").length;
  $number.attr('min', 1);
  $number.attr('max', pages);
  $number.attr('step', 1);
  $number.attr('value', PAGE);

  $number.on('input', function(){
    let val = $(this).val();
    let offTop = $("#page-"+val).offset().top - 50;
    $('html, body').scrollTop(offTop);
    sessionStorage.setItem("page", val);
  });

  return $container;
}

function createCheckboxControl(name, boolean){
  // html
  let $container = $('<div>').addClass('control');
  let $checkbox = $('<input>').attr('type', 'checkbox');
  let $label = $('<label>').html(name);
  $container.append($checkbox);
  $container.append($label);
  // toggle class on body, on state change
  $checkbox.on('change', function(){
    $('body').toggleClass(name);
    boolean = !boolean;
    sessionStorage.setItem(name, boolean);
  });
  // init
  if(boolean){
    $('body').addClass(name);
    $checkbox.prop('checked', true);
  }
  return $container;
}

// --- remember scroll values through reload

window.addEventListener('scroll', function(){
  if (INTERFACE_CREATED){
    let x = Math.round(window.scrollX);
    let y = Math.round(window.scrollY);
    sessionStorage.setItem("scrollX", x);
    sessionStorage.setItem("scrollY", y);
  }
})

// --- init

function initInterface(){
  console.log("interface-start");


  // add the interface elements
  let $interface = $("<section>").attr('class', 'interface');
  $interface.append(createCheckboxControl('spread', SPREAD));
  $interface.append(createCheckboxControl('preview', PREVIEW));
  $interface.append(createGoToPageButton());
  $interface.append(createZoomSlider());
  $('body').prepend($interface);

  // pagedjs fix
  $interface.css('position','fixed');

  // finally set scroll values from sessionStorage
  let scrollX = sessionStorage.getItem("scrollX");
  let scrollY = sessionStorage.getItem("scrollY");
  window.scrollTo(parseInt(scrollX), parseInt(scrollY));

  INTERFACE_CREATED = true;
  console.log("interface-end");
}
