
// mini GUY interface for pagedjs
// written by Dorian Timmermans

// DONE:
// * zoom level (that does not influence the interface)
// * save checkboxes state and scroll level through ctrl+R (put in url?)
// * interface outline have constant stroke width

// TODO:
// * go to page x button
// * start
// * don't spread if only 1 sheet
// * prevent classic zoom
// * more generic control object (class?) that automatically:
//  - update function on input, has default value, set in session storage


// GETTING BACK SESSION VARIABLES
// ============================================================================

// reload value from sessionStorage
// IMPORTANT NOTE: for that to work we have to call this function after pagedjs
// using the window.PagedConfig after parameter
// handler/hook or calling pagedjs async does not work!
function getSessionValue(valueName, defaultValue){
  let sessionValue = JSON.parse(sessionStorage.getItem(valueName));
  return sessionValue === null ? defaultValue : sessionValue;
}

let SPREAD = getSessionValue("spread", false);
let PREVIEW = getSessionValue("preview", false);
let ZOOM = getSessionValue("zoom", 1);
let PAGE = getSessionValue("page", 1);

let INTERFACE_CREATED = false;


// CREATING CONTROLS
// ============================================================================

// generic control html
function createControl(name, type, title){

  // html
  let $container = $('<div>').addClass('control');
  // TODO: normally would do an ID, by we can't target id with pagedjs outside the pages??
  $container.addClass(name+'-control');

  $control = $('<input>').attr('type', type).attr('title', title);

  if(type == "button"){
    $control.attr("value", name);
  }
  else{
    let $label = $('<label>').html(name);
    $container.append($label);
  }
  $container.prepend($control);


  return $container;
}

function createCheckboxControl(name, boolean){
  // html
  let $container = createControl(name, 'checkbox');
  let $checkbox = $container.find("input");

  // toggle class on body, on state change
  $checkbox.on('change', function(){
    $('body').toggleClass(name);
    boolean = !boolean;
    sessionStorage.setItem(name, boolean);
  });

  // init values
  if(boolean){
    $('body').addClass(name);
    $checkbox.prop('checked', true);
  }

  return $container;
}

function createZoomSlider(){

  // html
  let $container = createControl('zoom', 'range');
  let $slider = $container.find("input");

  // init values
  $slider.attr('min', 0);
  $slider.attr('max', 2);
  $slider.attr('step', 0.05);
  $slider.attr('value', ZOOM);

  let $pages = $('.pagedjs_pages');
  $pages.css("transform-origin", "top left");

  function updateZoomSlider(val){
    // apply zoom
    $pages.css("transform", "scale(" + val + ")");
    // apply reverse zoom to interface outlines
    let val_inv = 1/val;
    $("html").attr("style","--interface-outline:"+ val_inv +"px");
    // set sessionStorage
    sessionStorage.setItem("zoom", val);
  }

  $slider.on('input', function(){
      let val = $(this).val();
      updateZoomSlider(val);
  });

  updateZoomSlider(ZOOM);

  return $container;
}


function createGoToPageButton(){
  // Note:
  // no need for sessionStorage because pages is already remembered by
  // scroll, but for button coherence so the value is the good one when refreshing

  // html
  let $container = createControl('page', 'number');
  let $number = $container.find("input");

  // values
  let pages = $(".pagedjs_page").length;
  $number.attr('min', 1);
  $number.attr('max', pages);
  $number.attr('step', 1);
  $number.attr('value', PAGE);

  $number.on('input', function(){
    let val = $(this).val();
    let offTop = $("#page-"+val).offset().top - 50;
    let offLeft = $("#page-"+val).offset().left - 50;
    $('html, body').scrollTop(offTop);
    $('html, body').scrollLeft(offLeft);
    sessionStorage.setItem("page", val);
  });

  return $container;
}

function createResolutionButton(){
  // nothing to remember here by the session

  // html
  let title = "log the resolution of each <img> and background-image in the console";
  let $container = createControl('resolution', 'button', title);
  let $number = $container.find("input");

  $number.on('click', function(){
    getResolutions();
  });

  return $container;

}

// ANALYSE IMAGES
// ============================================================================

function computeImageDPI(srcSize, size, px_per_inch){
  // nw and nh are the source file size in px
  // w and h are the html element size in px
  // pw and ph are the size in which is it printing in cm

  // html conversion between pixel unit and cm is 96dpi
  // TODO: does this conversion change according to the user DPI ???!!!
  // let pw = (size[0] / 96) * 2.54;
  // let ph = (size[1] / 96) * 2.54;

  let dpiw = srcSize[0] / (size[0] / px_per_inch);
  let dpih = srcSize[1] / (size[1] / px_per_inch);

  return [dpiw, dpih];
}

function getIMGDPI(img, px_per_inch){

  // original file size
  let srcSize = [img.naturalWidth, img.naturalHeight];
  // size in px without border
  let size = [img.clientWidth, img.clientHeight];

  let dpi = computeImageDPI(srcSize, size, px_per_inch);

  console.log("-----------");
  console.log(img.src);
  console.log("type:", "<img>");
  console.log("src size", srcSize);
  console.log("size", size.map(x => Math.round(x)));
  console.log("dpi", dpi.map(x => Math.round(x)));
}

function getBackgroundImageDPI(container, px_per_inch){

  let backgroundIMG = $(container).css('background-image');

  if(   backgroundIMG.includes("url")
    && !backgroundIMG.includes("data:image/svg+xml")){

    let imageSrc = backgroundIMG.replace(/url\((['"])?(.*?)\1\)/gi, '$2').split(',')[0];

    // TODO: for the moment we are only taking the first one!

    // get natural width and height
    let image = new Image();
    image.src = imageSrc;

    image.onload = function () {

      let srcSize = [image.width, image.height];
      let ratio = srcSize[0]/srcSize[1];
      let containerSize = [container.clientWidth, container.clientHeight];

      // parsing backgroundSize
      // TODO: only taking the % and px case for now
      let backgroundSize = $(container).css('background-size');
      backgroundSize = backgroundSize.split(" ");
      backgroundSize[1] = backgroundSize[1] ? backgroundSize[1] : "auto";
      let size = [];

      // compute the pixel value
      for (var i = 0; i < backgroundSize.length; i++) {
        let current = backgroundSize[i];
        let other = backgroundSize[(i+1)%2];

        if (current.includes("%")){
          size[i] = (parseInt(current)/100) * containerSize[i];
        }
        if (current.includes("px")){
          size[i] = parseInt(current);
        }
        else if (current == "auto"){
          size[i] = "auto";
        }
      }

      // complete the auto based on ratio
      if(size[0] == "auto" && size[1] == "auto"){
        size = srcSize;
      }
      if(size[0] == "auto"){
        size[0] = size[1] * ratio;
      }
      if(size[1] == "auto"){
        size[1] = size[0] / ratio;
      }

      let containerSize_cm = [containerSize[0] / px_per_inch, containerSize[1] / px_per_inch];

      let dpi = computeImageDPI(srcSize, size, px_per_inch);

      console.log("-----------");
      console.log(imageSrc);
      console.log("type:", "background-image");
      console.log("container size in px", containerSize);
      console.log("container size in cm", containerSize_cm.map(x => Math.round(x)));
      console.log("src size", srcSize);
      console.log("background size", backgroundSize);
      console.log("real size in px", size.map(x => Math.round(x)));
      console.log("dpi", dpi.map(x => Math.round(x)));
    };

  }
}

function getResolutions(){

  let page_width_px = $('.pagedjs_area').first().get()[0].clientWidth;
  // TODO: get from format
  let page_width_cm = 120;
  let px_per_inch = Math.round((page_width_px / page_width_cm) * 2.54);
  console.log("-----------");
  console.log("page width in px", page_width_px)
  console.log("page width in cm", page_width_cm)
  console.log("resolution in px/inch", px_per_inch)

  // for each img that is not an svg
  $('img').each(function(){

    if(!this.src.includes(".svg")){

      getIMGDPI(this, px_per_inch);
    }
  });

  // for each background-img that is not an svg
  $('.pagedjs_page *').each(function(){

    let backgroundIMG = $(this).css('background-image');

    if(   backgroundIMG.includes("url")
      && !backgroundIMG.includes("data:image/svg+xml")){

        getBackgroundImageDPI(this, px_per_inch);
    }
  });
}

// REMEBER SCROLL POSITION
// ============================================================================

window.addEventListener('scroll', function(){
  if (INTERFACE_CREATED){
    let x = Math.round(window.scrollX);
    let y = Math.round(window.scrollY);
    sessionStorage.setItem("scrollX", x);
    sessionStorage.setItem("scrollY", y);
  }
})


// INIT INTERFACE
// ============================================================================

function initInterface(){
  console.log("interface-start");

  // add the interface elements
  let $interface = $("<section>").attr('class', 'interface');

  // affichage
  $interface.append(createCheckboxControl('spread', SPREAD));
  $interface.append(createCheckboxControl('preview', PREVIEW));
  $interface.append(createZoomSlider());

  // move
  $interface.append(createGoToPageButton());

  // info
  $interface.append(createResolutionButton());

  $('body').prepend($interface);

  // pagedjs fix
  $interface.css('position','fixed');

  // finally set scroll values from sessionStorage
  let scrollX = sessionStorage.getItem("scrollX");
  let scrollY = sessionStorage.getItem("scrollY");
  window.scrollTo(parseInt(scrollX), parseInt(scrollY));

  console.log("interface-end");
  INTERFACE_CREATED = true;
}
