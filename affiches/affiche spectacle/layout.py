
from jinja2 import Template, Environment, BaseLoader, FileSystemLoader, environment
import yaml
from yaml.loader import SafeLoader
import os
import unidecode
import copy

CONTENT = 'content/'
TEMPLATE_PATH = 'template.html'
# HTML_PATH = 'index.html'

AFFICHE_METRO = 'metro.html'
AFFICHE_TOTEM = 'totem.html'
AFFICHE_VERTICAL = 'vertical.html'


def titleToId(title):
    split = title.split()
    new = []
    for word in split:
        #remove non-alphanum
        word = (x for x in word if x.isalnum())
        word = "".join(word)
        #only keep ascii and put in lower
        word = unidecode.unidecode(word).lower()
        new.append(word)
    return "-".join(new)


if __name__ == '__main__':

    # filter
    env = Environment(loader=FileSystemLoader(""))
    env.filters["slug"] = titleToId

    with open(TEMPLATE_PATH, 'r') as file:
        template_str = file.read()

    # we have to use the FileSystemLoader to be able to use include:
    template = env.from_string(template_str)

    content_metro = []
    content_totem = []
    content_vertical = []

    # parse yaml files
    for filename in os.listdir(CONTENT):
        filepath = os.path.join(CONTENT, filename)
        with open(filepath, 'r') as file:
            yaml_data = yaml.load(file, Loader=SafeLoader)
            if "metro" in yaml_data["orientation"]:
                content_metro.append(yaml_data)
            if "totem" in yaml_data["orientation"]:
                content_totem.append(yaml_data)
            if "vertical" in yaml_data["orientation"]:
                content_vertical.append(yaml_data)

    # generate metro html
    metro_html = template.render(affiches = content_metro, type = 'metro')
    with open(AFFICHE_METRO, 'w') as file:
        file.write(metro_html)

    # generate totem html
    totem_html = template.render(affiches = content_totem, type = 'totem')
    with open(AFFICHE_TOTEM, 'w') as file:
        file.write(totem_html)

    # generate vertical html
    vertical_html = template.render(affiches = content_vertical, type = 'vertical')
    with open(AFFICHE_VERTICAL, 'w') as file:
        file.write(vertical_html)

    print('Layout generated')
