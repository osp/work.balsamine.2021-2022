## how to

to launch a server (for paged.js to work)
```
python3 -m http.server
```

to generate `metro.html` & `totem.html`
```
python3 layout.py
```

change things in the `adjuments.css` using the id of the poster you want to adjust.

## things that could be better

* have a low and high img switch in the interface? (with two src)
* have real fond perdu happening in physical space for totem format
